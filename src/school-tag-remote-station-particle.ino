/*
 * Project school-tag-remote-station-particle
 * Description: NFC Stations along the Safe Routes to School.
 * Author: aaron.roller@aawhere.com
 * Date: 2019-11-10
 *
 * Particle.io implmentation of the remote station that writes visits to NFC
 * enabled School Tags for student rewards when they reach to School Station.
 *
 *  1. RFID Reader detects a tag
 *  2. NFC writes visit (station id, timestamp, station diagnostics like battery
 * level)
 *  3. Buzzer makes a sound indicating successful write
 *  4. LED lights indicating successful write
 *  5. System goes into low power mode to reduce power and will be woken by a
 * visiting tag.
 */

#include <MFRC522.h>
#include <StationSound.h>
#include <TagIo.h>
#include <stdio.h>
#include <time.h>

// MFRC522
#define SS_PIN A2
#define RST_PIN A1

// communicates to player
#define BUZZER_PIN D8

// useful for debugging
#define LED_PIN D7

// avoids compile error even though this is an enum declared in TagIo
class BatteryLevel;

MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance.
TagIo io(mfrc522);                // School Tag communication with tag
StationSound sound(BUZZER_PIN);

SYSTEM_MODE(MANUAL); // no cloud, save power

LEDStatus ledStatus(0x000000, LED_PATTERN_SOLID, LED_SPEED_NORMAL,
                    LED_PRIORITY_CRITICAL);
// used for sleeping
long timeOfLastVisit;
bool sleeping = false;
// setup() runs once, when the device is first turned on.
void setup() {
  pinMode(RST_PIN, OUTPUT); // used to power down the MFRC522
  pinMode(LED_PIN, OUTPUT); // provides feedback that a scan is taking place
  pinMode(BATT, INPUT);     // checks voltage

  Serial.begin(115200); // Initialize serial communications with the PC
  SPI.begin();          // Init SPI bus

  mfrc522.setSPIConfig();
  mfrc522.PCD_Init(); // Init MFRC522 card

  // stay awake upon booting for normal wake time
  timeOfLastVisit = Time.now();
  ledStatus.setActive(true);
  setTimeFromBuild();
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  manageSleep();
  writeVisitToTagIfPresent();
}

/**
 * Converts the particle voltage check into the battery levels allowed in TagIo.
 */
TagIo::BatteryLevel voltageToBatteryLevel() {
  // https://docs.particle.io/reference/device-os/firmware/xenon/#battery-voltage
  double voltage = analogRead(BATT) * 0.0011224;
  Serial.print(F("Voltage: "));
  Serial.println(voltage, DEC);

  if (voltage > 3.7) {
    return TagIo::BatteryLevel::VOLTAGE_HIGH;
  }
  if (voltage > 3.5) {
    return TagIo::BatteryLevel::VOLTAGE_MEDIUM;
  }
  if (voltage > 3.3) {
    return TagIo::BatteryLevel::VOLTAGE_LOW;
  }
  return TagIo::BatteryLevel::RECHARGE;
}

/**
 * Translates the Particle device id into an int using a hashcode algorithm.
 * Duplicates are possible, but rare.
 */
int stationId() {
  String deviceId = System.deviceID();
  int stationId = 0;
  for (unsigned int i = 0; i < deviceId.length(); i++) {
    // similar to java hashcode https://javadevnotes.com/java-string-hashcode
    stationId = 31 * stationId + deviceId.charAt(i);
  }
  return stationId;
}

/**
 * Reads new tags held to the RFID reader.
 * Writes a visit to the tag.
 * Provides feedback with sound and light for success/failure.
 */
void writeVisitToTagIfPresent() {
  if (io.tagIsReady()) {
    timeOfLastVisit = Time.now();
    if (!io.tagIsNew()) {
      sound.ofRepeatTag();
      return;
    }
  } else {
    return;
  }
  digitalWrite(LED_PIN, HIGH);

  TagIo::BatteryLevel battery = voltageToBatteryLevel();
  Serial.print(F("Battery: "));
  Serial.println(battery, HEX);

  TagIo::Visit visit = TagIo::Visit();
  visit.timestamp = Time.now();
  visit.stationId = stationId();
  visit.batteryLevel = battery;

  int indexOfWrite = io.writeVisit(visit);
  if (indexOfWrite < 0) {
    Serial.println(F("Write visit failed"));
    // FIXME: Write failure gets its own sound
    sound.ofRepeatTag();
  } else {
    sound.ofSuccessfulVisit(indexOfWrite + 1);
  }

  io.print(); // FIXME: remove deubgging

  sound.ofSuccessfulVisit(io.numberOfVisits());
  digitalWrite(LED_PIN, LOW);
}

/**
 * call this to set the time from the local time of the build.  The clock must
 * remain on with no interruptions in power since every boot will use the build
 * time. In case of power interruption, you must flash again to get the local
 * build time.
 */
void setTimeFromBuild() {

  struct tm tm;

  String timestamp = String(__DATE__);
  timestamp.concat(String(" "));
  timestamp.concat(__TIME__);
  time_t localBuildTime;
  if (strptime(timestamp, "%b %d %Y %H:%M:%S", &tm) != NULL) {
    localBuildTime = mktime(&tm);
    long timeZoneOffsetSeconds = 8 * 3600; // Pacific Standard Time
    Time.setTime(localBuildTime + timeZoneOffsetSeconds);
  } else {
    Serial.print("Failure to parse timestamp ");
    Serial.println(timestamp);
  }
}

/**Puts the system to sleep if inactive or keeps awake if just woken from
 * sleep.*/
void manageSleep() {
  if (!sleeping) {
    sleepIfInactive();
  } else {
    justWokeUp();
  }
}
/**
 * puts to sleep if time awake since last visit is too long.*/
void sleepIfInactive() {
  long timeAwake = Time.now() - timeOfLastVisit;
  if (timeAwake > 15) {
    sleeping = true;
    Serial.print("sleeping after");
    Serial.println(timeAwake);
    Serial.print(analogRead(BATT) * 0.0011224);
    Serial.println(" Volts");
    PCD_SoftPowerDown();
    System.sleep({SS_PIN, MISO, MOSI, SCK}, CHANGE);
  } else {
    Serial.print(timeAwake, DEC);
    Serial.print(" ");
  }
}

/** called when something triggered a wakeup.  Prepares for being awake.
 *
 */
void justWokeUp() {

  Serial.println("already sleeping");

  SleepResult result = System.sleepResult();
  switch (result.reason()) {
  case WAKEUP_REASON_NONE: {
    Serial.println("Xenon did not wake up from sleep");
    break;
  }
  case WAKEUP_REASON_PIN: {
    Serial.println("Xenon was woken up by a pin");
    sleeping = false;
    break;
  }
  case WAKEUP_REASON_RTC: {
    Serial.println("Xenon was woken up by the RTC (after a specified number of "
                   "seconds)");
    break;
  }
  case WAKEUP_REASON_PIN_OR_RTC: {
    Serial.println("Xenon was woken up by either a pin or the RTC (after a "
                   "specified number of seconds)");
    break;
  }
  default:
    Serial.print("Didn't match a wake up reason.: ");
    Serial.println(result.reason());
  }
}

/**
 * Copied from Arduino library, this may help in power reduction.  It would be
 * nice to prove it uses less power.
 * https://github.com/miguelbalboa/rfid/pull/334/files
 */
void PCD_SoftPowerDown() { // Note : Only soft power down mode is available
                           // throught software
  byte val = mfrc522.PCD_ReadRegister(
      MFRC522::CommandReg); // Read state of the command register
  val |= (1 << 4);          // set PowerDown bit ( bit 4 ) to 1
  mfrc522.PCD_WriteRegister(MFRC522::CommandReg,
                            val); // write new value to the command register
}

void PCD_SoftPowerUp() {
  byte val = mfrc522.PCD_ReadRegister(
      MFRC522::CommandReg); // Read state of the command register
  val &= ~(1 << 4);         // set PowerDown bit ( bit 4 ) to 0
  mfrc522.PCD_WriteRegister(MFRC522::CommandReg,
                            val); // write new value to the command register
  // wait until PowerDown bit is cleared (this indicates end of wake up
  // procedure)
  const uint32_t timeout =
      (uint32_t)millis() + 500; // create timer for timeout (just in case)

  while (millis() <= timeout) { // set timeout to 500 ms
    val = mfrc522.PCD_ReadRegister(
        MFRC522::CommandReg); // Read state of the command register
    if (!(val & (1 << 4))) {  // if powerdown bit is 0
      break;                  // wake up procedure is finished
    }
  }
}
