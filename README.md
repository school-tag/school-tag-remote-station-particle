# Station Tag Remote Station - Particle

[School Tag](https://schooltag.org) stations are battery powered RFID readers of NFC School Tags that are placed along the [Safe Routes to Schools](http://saferoutestoschools.org/).

This is the [Particle.io](https://particle.io) implementation of a remote station.
See the [Arduino implementation](https://bitbucket.org/school-tag/school-tag-remote-station-arduino) for a comparison.

# Setup

## Software IDE

This is a standard Particle.io project created using the [Particle Workbench](https://www.particle.io/workbench/) with Visual Studio Code. See [School Tag Station common][common] for more about IDE setup.

## Source Code

There is one source file to run the station:

`src/school-tag-remote-station-particle.ino`

It uses Arduino style coding to produce C++ code for the Particle board.

## Libraries

Library dependencies are referenced in `project.properties` so the following is information only .

### School Tag Common Library

The source code is greatly simplified by importing the [School Tag Station common][common] code. Do this using the
_Particle Workbench -> Install Library_ and entering:

`SchoolTagStationCommon`

### MFRC522 RFID Library

`MFRC522` library is used to communicate with the MFRC522 RFID card that scans the NFC School Tags.

## Hardware

You'll need a [Particle Mesh](https://www.particle.io/mesh) board, specfically the [Particle Xenon](https://store.particle.io/products/xenon-kit) is most affordable and fully functional for the station's needs.

See [school-tag-station-particle](https://bitbucket.org/school-tag/school-tag-station-particle) for more information about hookup. This documentation will be updated with more robust implementation of the station.

[common]: https://bitbucket.org/school-tag/school-tag-station-common

### Real Time Clock

The clock is set upon firmware write to the station.  The station must remained powered for the time to remain active.

Particle.io provides a clock best explained by the [Time](https://docs.particle.io/reference/device-os/firmware/xenon/#time) docs.  Since Particle.io is a cloud system, the time is usually set by the cloud keeping it always in sync. The
station does not depend on the cloud so without connectivity, the clock is not set by the cloud.  


### Station ID

Particle.io boards come with a unique [Device ID](https://docs.particle.io/reference/device-os/firmware/xenon/#deviceid-). This ID consumes too many bytes to send directly in the School Tag.  Instead, a simple hash code algorithm converts the 
long string into a two byte station ID.  Duplicates are possible, but unlikely at a single game deployment. 

### Battery Level

Particle.io boards provide the ability to read [voltage](https://docs.particle.io/reference/device-os/firmware/xenon/#battery-voltage) which indicates battery strength.  The operating voltage is 3.3V so when it reaches that level, an
`Empty` signal is reported in the standard diagnostics `Word` of the station visit.  

